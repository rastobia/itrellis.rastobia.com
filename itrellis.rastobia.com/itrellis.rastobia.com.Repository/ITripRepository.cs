﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using itrellis.rastobia.com.Model;

namespace itrellis.rastobia.com.Repository
{
    public interface ITripRepository
    {
        List<Trip> GetTripInformationByTripID(int p_tripID);

        List<Trip> GetTripInformationByTravellerID(int p_travellerID);

        Traveller GetTravellerInformationByTravellerID(int p_travellerID);

        List<Category> GetAllCategories();

        int? InsertTripItemInformation(TripItems p_item);

        int? InsertTravellerInformation(Traveller p_traveller);

        int? InsertTripInformation();
    }
}
