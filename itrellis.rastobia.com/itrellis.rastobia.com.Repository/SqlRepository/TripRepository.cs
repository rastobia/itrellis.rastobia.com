﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

using log4net;

using itrellis.rastobia.com.Model;
using itrellis.rastobia.com.Common;

namespace itrellis.rastobia.com.Repository.SqlRepository
{
    public class TripRepository : ITripRepository
    {
        #region Globals

        private static log4net.ILog _logger;
        private string _connectionString { get; set; }
        private int _sqlCommandTimeout { get; set; }

        public TripRepository(string p_connectionString, int p_sqlCommandTimeout)
        {
            this._connectionString = p_connectionString;
            this._sqlCommandTimeout = p_sqlCommandTimeout;

            if (_logger == null)
            {
                Type type = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                _logger = log4net.LogManager.GetLogger(type.Name);
            }
        }

        #endregion

        #region Create

        public int? InsertTripInformation()
        {
            int? output = null;

            SqlCommand command = new SqlCommand();
            command.CommandText = @"INSERT INTO [Trip]
           ([CreateStamp])
     VALUES
           (GETDATE());
SELECT CAST(SCOPE_IDENTITY() AS INT) AS TripID";

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    command.Connection = con;
                    command.Connection.Open();
                    output = command.ExecuteScalar() as int? ?? null;
                    command.Connection.Close();
                }
            }
            catch (Exception ex)
            {
                _logger.Fatal("InsertTripInformation", ex);

                throw ex;
            }

            return output;
        }

        public int? InsertTravellerInformation(Traveller p_traveller)
        {
            int? output = null;

            SqlCommand command = new SqlCommand();
            command.CommandText = @"INSERT INTO [Travellers]
           ([FirstName], 
[LastName])
     VALUES
           (@first, 
@last);
SELECT CAST(SCOPE_IDENTITY() AS INT) AS TravellerID";


            command.Parameters.AddWithValue("@first", SqlParse.ParseValues(p_traveller.FirstName));
            command.Parameters.AddWithValue("@last", SqlParse.ParseValues(p_traveller.LastName));

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    command.Connection = con;
                    command.Connection.Open();
                    output = command.ExecuteScalar() as int? ?? null;
                    command.Connection.Close();
                }
            }
            catch (Exception ex)
            {
                _logger.Fatal("InsertTravellerInformation", ex);

                throw ex;
            }

            return output;
        }

        public int? InsertTripItemInformation(TripItems p_item)
        {
            int? output = null;

            SqlCommand command = new SqlCommand();
            command.CommandText = @"INSERT INTO [TripItems]
           (TripID, 
TravellerID, 
Amount,
Currency,
Description,
CreateStamp)
     VALUES
           (@trip, 
@traveller,
@amount,
@currency,
@description, 
GETDATE());
SELECT CAST(SCOPE_IDENTITY() AS INT) AS ID";


            command.Parameters.AddWithValue("@trip", SqlParse.ParseValues(p_item.TripID));
            command.Parameters.AddWithValue("@traveller", SqlParse.ParseValues(p_item.TravellerID));
            command.Parameters.AddWithValue("@amount", SqlParse.ParseValues(p_item.Amount));
            command.Parameters.AddWithValue("@currency", SqlParse.ParseValues(p_item.Currency));
            command.Parameters.AddWithValue("@description", SqlParse.ParseValues(p_item.Description));

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    command.Connection = con;
                    command.Connection.Open();
                    output = command.ExecuteScalar() as int? ?? null;
                    command.Connection.Close();
                }
            }
            catch (Exception ex)
            {
                _logger.Fatal("InsertTripItemInformation", ex);

                throw ex;
            }

            return output;
        }

        #endregion

        #region Retrieve

        #region Query

        private string TripColumns = @"Select t.TripID,
t.CreateStamp,
ti.Amount,
ti.Currency,
ti.Description,
ti.TripItemID,
tr.FirstName,
tr.LastName,
tr.TravellerID
FROM Trip t WITH(NOLOCK)
Inner Join TripItems ti WITH(NOLOCK) on t.TripID = ti.TripID
Left Outer Join Travellers tr WITH(NOLOCK) on tr.TravellerID = ti.TravellerID";

        private string TravellerColumns = @"SELECT tr.TravellerID, 
tr.FirstName, 
tr.LastName
FROM Travellers tr WITH(NOLOCK)";

        private string CategoryColumn = @"SELECT c.CategoryID, 
c.Description
FROM Category c WITH(NOLOCK)
ORDER BY Description";

        public Traveller GetTravellerInformationByTravellerID(int p_travellerID)
        {
            Traveller output = new Traveller();

            DataTable dt = new DataTable();

            SqlCommand command = new SqlCommand();
            command.CommandTimeout = _sqlCommandTimeout;

            command.CommandText = TravellerColumns + " WHERE tr.TravellerID = @id";

            command.Parameters.AddWithValue("@id", SqlParse.ParseValues(p_travellerID));

            _logger.Debug("GetTravellerInformationByTravellerID: " + SqlParse.GetFullCommandText(command));

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    command.Connection = con;
                    command.Connection.Open();
                    dt.Load(command.ExecuteReader());
                    command.Connection.Close();
                }
                output = ReadTraveller(dt).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.Error("GetTravellerInformationByTravellerID: " + SqlParse.GetFullCommandText(command), ex);

                throw ex;
            }

            return output;
        }

        public List<Category> GetAllCategories()
        {
            List<Category> output = new List<Category>();

            DataTable dt = new DataTable();

            SqlCommand command = new SqlCommand();
            command.CommandTimeout = _sqlCommandTimeout;

            command.CommandText = CategoryColumn;

            _logger.Debug("GetAllCategories: " + SqlParse.GetFullCommandText(command));

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    command.Connection = con;
                    command.Connection.Open();
                    dt.Load(command.ExecuteReader());
                    command.Connection.Close();
                }
                output = ReadCategories(dt);
            }
            catch (Exception ex)
            {
                _logger.Error("GetAllCategories: " + SqlParse.GetFullCommandText(command), ex);

                throw ex;
            }

            return output;
        }

        public List<Trip> GetTripInformationByTravellerID(int p_travellerID)
        {
            List<Trip> output = new List<Trip>();

            DataTable dt = new DataTable();

            SqlCommand command = new SqlCommand();
            command.CommandTimeout = _sqlCommandTimeout;

            command.CommandText = TripColumns + " WHERE tr.TravellerID = @id";

            command.Parameters.AddWithValue("@id", SqlParse.ParseValues(p_travellerID));

            _logger.Debug("GetTripInformationByTravellerID: " + SqlParse.GetFullCommandText(command));

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    command.Connection = con;
                    command.Connection.Open();
                    dt.Load(command.ExecuteReader());
                    command.Connection.Close();
                }
                output = ReadTripInformation(dt);
            }
            catch (Exception ex)
            {
                _logger.Error("GetTripInformationByTravellerID: " + SqlParse.GetFullCommandText(command), ex);

                throw ex;
            }

            return output;
        }

        public List<Trip> GetTripInformationByTripID(int p_tripID)
        {
            List<Trip> output = new List<Trip>();

            DataTable dt = new DataTable();

            SqlCommand command = new SqlCommand();
            command.CommandTimeout = _sqlCommandTimeout;

            command.CommandText = TripColumns + " WHERE t.TripID = @id";

            command.Parameters.AddWithValue("@id", SqlParse.ParseValues(p_tripID));

            _logger.Debug("GetTripInformationByTripID: " + SqlParse.GetFullCommandText(command));

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    command.Connection = con;
                    command.Connection.Open();
                    dt.Load(command.ExecuteReader());
                    command.Connection.Close();
                }
                output = ReadTripInformation(dt);
            }
            catch (Exception ex)
            {
                _logger.Error("GetTripInformationByTripID: " + SqlParse.GetFullCommandText(command), ex);

                throw ex;
            }

            return output;
        }

        #endregion

        #region Parse

        /// </summary>
        /// <param name="p_dt">Datatable</param>
        /// <returns></returns>
        private List<Category> ReadCategories(DataTable p_dt)
        {
            List<Category> output = new List<Category>();

            foreach (DataRow dr in p_dt.Rows)
            {
                Category c = new Category();

                c.CategoryID = dr["CategoryID"] as int? ?? null;
                c.Description = dr["Description"].ToString();

                output.Add(c);
            }

            return output;
        }

        /// </summary>
        /// <param name="p_dt">Datatable</param>
        /// <returns></returns>
        private List<Trip> ReadTripInformation(DataTable p_dt)
        {
            List<Trip> output = new List<Trip>();

            foreach (DataRow dr in p_dt.Rows)
            {
                Trip t = new Trip();

                t.CreateStamp = dr["CreateStamp"] as DateTime? ?? null;

                t.Item = new TripItems();
                t.Item.Amount = dr["Amount"] as decimal? ?? null;
                t.Item.Currency = dr["Currency"].ToString();
                t.Item.Description = dr["Description"].ToString();
                t.Item.TripItemID = dr["TripItemID"] as int? ?? null;

                t.Traveller = new Traveller();
                t.Traveller.FirstName = dr["FirstName"].ToString();
                t.Traveller.LastName = dr["LastName"].ToString();
                t.Traveller.TravellerID = dr["TravellerID"] as int? ?? null;

                t.TripID = dr["TripID"] as int? ?? null;

                output.Add(t);
            }

            return output;
        }

        /// </summary>
        /// <param name="p_dt">Datatable</param>
        /// <returns></returns>
        private List<Traveller> ReadTraveller(DataTable p_dt)
        {
            List<Traveller> output = new List<Traveller>();

            foreach (DataRow dr in p_dt.Rows)
            {
                Traveller t = new Traveller();
                t.FirstName = dr["FirstName"].ToString();
                t.LastName = dr["LastName"].ToString();
                t.TravellerID = dr["TravellerID"] as int? ?? null;

                output.Add(t);
            }

            return output;
        }


        #endregion
    }

    #endregion  
}
