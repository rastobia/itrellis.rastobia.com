﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace itrellis.rastobia.com.Common
{
    public static class SqlParse
    {
        /// <summary>
        /// Will dynamically parse the SQL value types from object types
        /// </summary>
        /// <param name="input">object</param>
        /// <returns>object</returns>
        public static object ParseValues(object input)
        {
            if (input == null)
            {
                return DBNull.Value;
            }
            else if (input.GetType() == typeof(string))
            {
                if (!String.IsNullOrEmpty(input.ToString()))
                {
                    return input.ToString();
                }
                else
                {
                    return DBNull.Value;
                }
            }
            else if (input.GetType() == typeof(decimal?))
            {
                if (input != null)
                {
                    return input;
                }
                else
                {
                    return DBNull.Value;
                }
            }
            else if (input.GetType() == typeof(decimal))
            {
                return input;
            }
            else if (input.GetType() == typeof(int?))
            {
                if (input != null)
                {
                    return input;
                }
                else
                {
                    return DBNull.Value;
                }
            }
            else if (input.GetType() == typeof(int))
            {
                return input;
            }
            else if (input.GetType() == typeof(bool?))
            {
                if (input != null)
                {
                    return input;
                }
                else
                {
                    return DBNull.Value;
                }
            }
            else if (input.GetType() == typeof(bool))
            {
                return input;
            }
            else if (input.GetType() == typeof(DateTime))
            {
                if (input != null)
                {
                    return input;
                }
                else
                {
                    return DBNull.Value;
                }
            }
            else if (input.GetType() == typeof(double?))
            {
                if (input != null)
                {
                    return input;
                }
                else
                {
                    return DBNull.Value;
                }
            }
            else if (input.GetType() == typeof(double))
            {
                return input;
            }
            else
            {
                return DBNull.Value;
            }
        }

        /// <summary>
        /// Takes in a SqlCommand object and returns the complete sql query 
        /// </summary>
        /// <param name="command">SqlCommand object</param>
        /// <returns>string containing the full sql query including all parameters</returns>
        public static string GetFullCommandText(this SqlCommand command)
        {
            string query = command.CommandText;
            foreach (SqlParameter p in command.Parameters)
            {

                if (p.DbType == DbType.String || p.DbType == DbType.Date || p.DbType == DbType.DateTime || p.DbType == DbType.DateTime2)
                {
                    query = "'" + query.Replace(p.ParameterName, p.Value.ToString()) + "'";
                }
                else
                {
                    query = query.Replace("@" + p.ParameterName, p.Value.ToString());
                }
            }
            return query;
        }
    }
}
