﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using itrellis.rastobia.com.Model;
using itrellis.rastobia.com.Repository.SqlRepository;
using itrellis.rastobia.com.Service;

namespace itrellis.rastobia.com.Tests
{
    [TestClass]
    public class TripTests
    {
        private static string connectionString = Global.connectionString;

        private static TripService _tripService = new TripService(new TripRepository(connectionString, Global.SqlCommandTimeout));

        [TestMethod]
        public void InsertTravellerInfo()
        {
            Traveller test = new Traveller();

            test.FirstName = "Bilbo";
            test.LastName = "Baggins";

            int travellerID = _tripService.InsertTravellerInformation(test).GetValueOrDefault();

            Assert.IsNotNull(travellerID);
        }

        [TestMethod]
        public void InsertTripInfo()
        {
            int tripID = _tripService.InsertTripInformation().GetValueOrDefault();

            Assert.IsNotNull(tripID);
        }

        [TestMethod]
        public void InsertTripItemInfo()
        {
            TripItems test = new TripItems();

            test.Amount = 999;
            test.Currency = "DKK";
            test.Description = "Other";
            test.TravellerID = 1;
            test.TripID = 1;

            int tripItemID = _tripService.InsertTripItemInformation(test).GetValueOrDefault();

            Assert.IsNotNull(tripItemID);
        }


        [TestMethod]
        public void GetTravellerInfo()
        {
            Traveller test = new Traveller();

            test = _tripService.GetTravellerInformationByTravellerID(1);

            Assert.IsNotNull(test);
            Assert.IsNotNull(test.FirstName);
        }

        [TestMethod]
        public void GetTripInfoByTravellerID()
        {
            List<Trip> test = new List<Trip>();

            test = _tripService.GetTripInformationByTravellerID(1);

            Assert.IsNotNull(test);
            Assert.IsTrue(test.Count > 0);
            Assert.IsNotNull(test[0].TripID);
        }

        [TestMethod]
        public void GetTripInfoByTripID()
        {
            List<Trip> test = new List<Trip>();

            test = _tripService.GetTripInformationByTripID(21);

            Assert.IsNotNull(test);
            Assert.IsTrue(test.Count > 0);
            Assert.IsNotNull(test[0].TripID);
        }
    }
}
