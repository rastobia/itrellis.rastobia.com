﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using itrellis.rastobia.com.Model;
using itrellis.rastobia.com.Repository;
using itrellis.rastobia.com.Repository.SqlRepository;

namespace itrellis.rastobia.com.Service
{
    public class TripService
    {
        #region Globals

        private ITripRepository _sqlRepository { get; set; }

        public TripService(ITripRepository p_repository)
        {
            this._sqlRepository = p_repository;
        }

        #endregion

        public List<Trip> GetTripInformationByTripID(int p_tripID)
        {
            return _sqlRepository.GetTripInformationByTripID(p_tripID);
        }
        public Traveller GetTravellerInformationByTravellerID(int p_travellerID)
        {
            return _sqlRepository.GetTravellerInformationByTravellerID(p_travellerID);
        }

        public List<Trip> GetTripInformationByTravellerID(int p_travellerID)
        {
            return _sqlRepository.GetTripInformationByTravellerID(p_travellerID);
        }

        public List<Category> GetAllCategories()
        {
            return _sqlRepository.GetAllCategories();
        }

        public int? InsertTripItemInformation(TripItems p_item)
        {
            return _sqlRepository.InsertTripItemInformation(p_item);
        }

        public int? InsertTravellerInformation(Traveller p_traveller)
        {
            return _sqlRepository.InsertTravellerInformation(p_traveller);
        }

        public int? InsertTripInformation()
        {
            return _sqlRepository.InsertTripInformation();
        }
    }
}
