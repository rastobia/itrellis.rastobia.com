﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="itrellis.rastobia.com._Default" %>

<asp:Content ID="Body" ContentPlaceHolderID="MainContent" runat="server">
    <style>
    </style>

    <div id="divTripPlannerHeader" class="subPageHeader">
        Trip Planner
    </div>
    <br />
    <div class="subPageDisplay">
        <asp:Button ID="btnNewTrip" runat="server" Text="New Trip" PostBackUrl="~/NewTrip.aspx" />
        <p>A page where you can add a new trip, add to your current trip, submit your current trip for calculations, or clearing your current trip</p>
        <br />
        <br />

        <asp:Button ID="btnCalulateTrip" runat="server" Text="Calculate Trip" PostBackUrl="~/CalculateTrip.aspx" />
        <p>A page where you can see what the cost split for your trip is based on what has been entered on the New Trip page</p>
    </div>
</asp:Content>
