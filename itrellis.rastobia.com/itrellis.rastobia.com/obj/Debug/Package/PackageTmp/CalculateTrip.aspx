﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CalculateTrip.aspx.cs" Inherits="itrellis.rastobia.com.CalculateTrip" %>

<asp:Content ID="Body" ContentPlaceHolderID="MainContent" runat="server">
    <div id="divTripItemRepeater" runat="server" class="subPageDisplay" style="padding: 10px;">
        <div class="ulDisplay">
            <div class="ulHeader">
                <ul>
                    <li class="header">
                        <span class="column">Name</span>
                        <span class="column">Total Paid</span>
                        <span class="column">Amount Owed</span>
                        <span class="column">Traveller Owed Money To</span>
                    </li>
                    <li class="rptSeperator">
                        <span class="column"></span>
                        <span class="column"></span>
                        <span class="column"></span>
                        <span class="column"></span>
                    </li>
                </ul>
            </div>
            <div class="ulBody" style="width: 800px;">
                <ul id="ulTrip" style="height: 400px;">
                    <asp:Repeater ID="rptTripItems" runat="server" OnItemDataBound="SetTripRepeater">
                        <ItemTemplate>
                            <li id="liItem" runat="server">
                                <span class="column" style="text-align: left;">
                                    <asp:Literal ID="litName" runat="server" />
                                </span>
                                <span class="column" style="text-align: left;">
                                    <asp:Literal ID="litTotal" runat="server" /></span>
                                <span class="column" style="text-align: left; font-size: 12px;">
                                    <asp:Literal ID="litOwed" runat="server" /></span>
                                <span class="column" style="text-align: left; font-size: 12px;">
                                    <asp:Literal ID="litOwedTo" runat="server" /></span>
                            </li>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            <li class="rptSeperator">
                                <span class="column" style="padding-left: 10px;"></span>
                                <span class="column"></span>
                                <span class="column"></span>
                                <span class="column"></span>
                            </li>
                        </SeparatorTemplate>
                    </asp:Repeater>
                </ul>
            </div>
        </div>
        <asp:Label ID="lblMessageUser" runat="server" Visible="false" Style="color: Red;" />
    </div>
</asp:Content>
