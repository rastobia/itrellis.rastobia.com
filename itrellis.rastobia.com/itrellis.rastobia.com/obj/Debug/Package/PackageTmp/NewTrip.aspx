﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewTrip.aspx.cs" Inherits="itrellis.rastobia.com.NewTrip" %>

<asp:Content ID="Body" ContentPlaceHolderID="MainContent" runat="server">
    <style>
    </style>
    <br />
    <div id="divTraveller" class="subPageDisplay" runat="server">
        <table id="tblTraveller">
            <tr class="header">
                <td class="column">First Name</td>
                <td class="column">Last Name</td>
            </tr>
            <tr>
                <td class="column">
                    <asp:TextBox runat="server" ID="tbxFirstName" /></td>
                <td class="column">
                    <asp:TextBox runat="server" ID="tbxLastName" /></td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <div id="divTripItem" class="subPageDisplay" runat="server">
        <table id="tblTripItem">
            <tr class="header">
                <td class="column">Amount</td>
                <td class="column">Description</td>
                <td class="column"></td>
            </tr>
            <tr>
                <td class="column">
                    <asp:TextBox runat="server" ID="tbxAmount" /></td>
                <td class="column">
                    <asp:DropDownList runat="server" ID="ddlDescription">
                        <asp:ListItem Text="--Description--" Value="" />
                        <asp:ListItem Text="Accomodation" Value="Accomodation" />
                        <asp:ListItem Text="Entertainment" Value="Entertainment" />
                        <asp:ListItem Text="Flight" Value="Flight" />
                        <asp:ListItem Text="Food/Drink" Value="Food/Drink" />
                        <asp:ListItem Text="Gas" Value="Gas" />
                        <asp:ListItem Text="Goods" Value="Goods" />
                        <asp:ListItem Text="Other" Value="Other" />
                        <asp:ListItem Text="Transportation" Value="Transportation" />
                        <asp:ListItem Text="Services" Value="Services" />
                    </asp:DropDownList></td>
                <td class="column">
                    <asp:Button ID="btnTripItem" runat="server" OnClick="btnTripItem_Click" Text="Add Trip Item"></asp:Button>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <div id="divActionButtons" class="subPageDisplay">
        <asp:Button ID="btnTraveller" runat="server" OnClick="btnTraveller_Click" Text="Add Traveller to Trip"></asp:Button>
        <asp:Button ID="btnCalculate" runat="server" OnClick="btnCalculate_Click" Text="Calculate Trip Cost Distribution"></asp:Button>
        <asp:Button ID="btnClearTrip" runat="server" OnClick="btnClearTrip_Click" Text="Clear Trip"></asp:Button>
    </div>
    <div style="text-align: center">
        <asp:Label ID="lblMessageUser" runat="server" Visible="false" Style="color: Red;" />
    </div>
    <br />
    <br />
    <div id="divTripItemRepeater" runat="server" class="subPageDisplay" style="padding: 10px;">
        <div class="ulDisplay">
            <div class="ulHeader">
                <ul>
                    <li class="header">
                        <span class="column">Name</span>
                        <span class="column">Amount</span>
                        <span class="column">Item Description</span>
                        <span class="column">Date Entered</span>
                    </li>
                    <li class="rptSeperator">
                        <span class="column"></span>
                        <span class="column"></span>
                        <span class="column"></span>
                        <span class="column"></span>
                    </li>
                </ul>
            </div>
            <div class="ulBody" style="width: 800px;">
                <ul id="ulTrip" style="height: 400px;">
                    <asp:Repeater ID="rptTripItems" runat="server" OnItemDataBound="SetTripRepeater">
                        <ItemTemplate>
                            <li id="liItem" runat="server">
                                <span class="column" style="text-align: left;">
                                    <asp:Literal ID="litName" runat="server" />
                                </span>
                                <span class="column" style="text-align: left;">
                                    <asp:Literal ID="litAmount" runat="server" /></span>
                                <span class="column" style="text-align: left; font-size: 12px;">
                                    <asp:Literal ID="litDescription" runat="server" /></span>
                                <span class="column" style="text-align: left; font-size: 12px;">
                                    <asp:Literal ID="litDate" runat="server" /></span>
                            </li>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            <li class="rptSeperator">
                                <span class="column" style="padding-left: 10px;"></span>
                                <span class="column"></span>
                                <span class="column"></span>
                                <span class="column"></span>
                            </li>
                        </SeparatorTemplate>
                    </asp:Repeater>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
