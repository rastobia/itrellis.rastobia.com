﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;

using itrellis.rastobia.com.Model;
using itrellis.rastobia.com.Repository.SqlRepository;
using itrellis.rastobia.com.Service;

namespace itrellis.rastobia.com
{
    public partial class NewTrip : System.Web.UI.Page
    {
        private string connectionString = Global.connectionString;
        private static log4net.ILog _logger;

        private TripService _tripService;
        protected void Page_Load(object sender, EventArgs e)
        {
            _tripService = new TripService(new TripRepository(connectionString, Global.SqlCommandTimeout));

            if (Request.Cookies["travellerID"] != null)
            {
                divTraveller.Visible = false;
            }

            if (Request.Cookies["tripID"] == null)
            {
                int currentTripID = _tripService.InsertTripInformation().GetValueOrDefault();

                HttpCookie tripID = new HttpCookie("tripID");
                DateTime now = DateTime.Now;

                tripID.Value = currentTripID.ToString();
                tripID.Expires = now.AddHours(2);

                Response.Cookies.Add(tripID);
            }
            LoadTripItems();
        }

        protected void LoadTripItems()
        {
            List<Trip> currentTripItems = new List<Trip>();

            if (Request.Cookies["tripID"] != null)
            {
                if (!string.IsNullOrEmpty(Request.Cookies["tripID"].Value.ToString()))
                {
                    currentTripItems = _tripService.GetTripInformationByTripID(int.Parse(Request.Cookies["tripID"].Value));
                }
            }

            if (currentTripItems.Count == 0)
            {
                divTripItemRepeater.Visible = false;
            }

            rptTripItems.DataSource = null;
            rptTripItems.DataSource = currentTripItems;
            rptTripItems.DataBind();
        }

        protected void SetTripRepeater(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                try
                {
                    Trip t = (Trip)e.Item.DataItem;

                    Literal litName = (Literal)e.Item.FindControl("litName");
                    Literal litAmount = (Literal)e.Item.FindControl("litAmount");
                    Literal litDescription = (Literal)e.Item.FindControl("litDescription");
                    Literal litDate = (Literal)e.Item.FindControl("litDate");


                    litName.Text = t.Traveller.FirstName + " " + t.Traveller.LastName;
                    litAmount.Text = String.Format("{0:0.00}", t.Item.Amount);
                    litDescription.Text = t.Item.Description.ToString();
                    litDate.Text = String.Format("{0:MM/dd/yyyy hh:mm tt}", t.CreateStamp);

                }
                catch (Exception ex)
                {
                    _logger.Fatal("SetTripRepeater", ex);

                    throw ex;
                }
            }
        }

        protected void btnTripItem_Click(object sender, EventArgs e)
        {
            decimal tryDecimal;

            if ((!string.IsNullOrEmpty(tbxFirstName.Text) || (Request.Cookies["travellerID"] != null && !string.IsNullOrEmpty(Request.Cookies["travellerID"].Value.ToString()))) && !string.IsNullOrEmpty(tbxAmount.Text) && decimal.TryParse(tbxAmount.Text, out tryDecimal))
            {
                if (!string.IsNullOrEmpty(tbxFirstName.Text))
                {
                    Traveller t = new Traveller();
                    t.FirstName = tbxFirstName.Text;
                    t.LastName = string.IsNullOrEmpty(tbxLastName.Text) ? "" : tbxLastName.Text;

                    int currentTravellerID = _tripService.InsertTravellerInformation(t).GetValueOrDefault();

                    HttpCookie travellerID = new HttpCookie("travellerID");
                    DateTime now = DateTime.Now;

                    travellerID.Value = currentTravellerID.ToString();
                    travellerID.Expires = now.AddHours(2);

                    Response.Cookies.Add(travellerID);

                }
                TripItems item = new TripItems();

                item.Amount = decimal.Parse(tbxAmount.Text.ToString());
                item.Currency = "USD";
                item.Description = ddlDescription.SelectedValue.ToString();
                item.TravellerID = int.Parse(Request.Cookies["travellerID"].Value.ToString());
                item.TripID = int.Parse(Request.Cookies["tripID"].Value.ToString());

                int tripItemID = _tripService.InsertTripItemInformation(item).GetValueOrDefault();

                Response.Redirect(Request.RawUrl);
            }
            else if (string.IsNullOrEmpty(tbxFirstName.Text) && ((Request.Cookies["travellerID"] == null || string.IsNullOrEmpty(Request.Cookies["travellerID"].Value.ToString()))))
            {
                lblMessageUser.Text = "Please enter first name";
                lblMessageUser.Visible = true;
            }
            else
            {
                lblMessageUser.Text = "Please enter a valid amount";
                lblMessageUser.Visible = true;
            }
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            if (rptTripItems.Items.Count > 0)
            {
                Response.Redirect("CalculateTrip.aspx");
            }else
            {
                lblMessageUser.Text = "There are no items to calculate.";
                lblMessageUser.Visible = true;
            }
        }

        protected void btnTraveller_Click(object sender, EventArgs e)
        {
            if (Request.Cookies["travellerID"] != null)
            {
                Response.Cookies["travellerID"].Expires = DateTime.Now.AddHours(-2);
            }
            Response.Redirect(Request.RawUrl);
        }

        protected void btnClearTrip_Click(object sender, EventArgs e)
        {
            if (Request.Cookies["tripID"] != null)
            {
                Response.Cookies["tripID"].Expires = DateTime.Now.AddHours(-2);
            }
            if (Request.Cookies["travellerID"] != null)
            {
                Response.Cookies["travellerID"].Expires = DateTime.Now.AddHours(-2);
            }
            Response.Redirect(Request.RawUrl);
        }
    }
}