﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace itrellis.rastobia.com
{
    public class Global : HttpApplication
    {
        public static string connectionString = ConfigurationManager.ConnectionStrings["live"].ConnectionString;

        public static int SqlCommandTimeout = int.Parse(ConfigurationManager.AppSettings["SqlCommandTimeout"]);

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}