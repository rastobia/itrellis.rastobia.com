﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using itrellis.rastobia.com.Model;
using itrellis.rastobia.com.Repository.SqlRepository;
using itrellis.rastobia.com.Service;

namespace itrellis.rastobia.com
{
    public partial class CalculateTrip : System.Web.UI.Page
    {
        private string connectionString = Global.connectionString;
        private TripService _tripService;
        private static log4net.ILog _logger;

        protected void Page_Load(object sender, EventArgs e)
        {
            _tripService = new TripService(new TripRepository(connectionString, Global.SqlCommandTimeout));

            if (Request.Cookies["tripID"] != null && !string.IsNullOrEmpty(Request.Cookies["tripID"].Value.ToString()))
            {
                CalculateTripTotals();
            }
            else
            {
                lblMessageUser.Text = "No valid trips available for calculation. Please try entering your trip again.";
                lblMessageUser.Visible = true;
            }
        }

        protected void CalculateTripTotals()
        {
            List<Trip> trip = _tripService.GetTripInformationByTripID(int.Parse(Request.Cookies["tripID"].Value));

            if (trip.Count > 0)
            {

                List<Trip> distinctTravellers = trip.GroupBy(x => x.Traveller.TravellerID).Select(y => y.First()).ToList();

                List<TripCalculations> calculations = new List<TripCalculations>();

                decimal averageCost = trip.Sum(a => a.Item.Amount).GetValueOrDefault() / distinctTravellers.Count();

                foreach (Trip t in distinctTravellers)
                {
                    List<Trip> allTravellerEntries = trip.Where(a => a.Traveller.TravellerID == t.Traveller.TravellerID).ToList();

                    decimal travellerTotal = decimal.Parse(allTravellerEntries.Sum(a => a.Item.Amount).ToString());

                    Traveller calculatedTotal = t.Traveller;

                    calculatedTotal.TotalPaid = travellerTotal;

                    TripCalculations calcTrip = new TripCalculations();

                    calcTrip.AveragePaidByTravellers = averageCost;
                    calcTrip.NumberOfTravellers = distinctTravellers.Count();
                    calcTrip.Traveller = calculatedTotal;

                    calculations.Add(calcTrip);
                }

                calculations = calculations.OrderByDescending(a => a.Traveller.TotalPaid).ToList();

                foreach (TripCalculations c in calculations)
                {
                    c.BiggestSpender = !string.IsNullOrEmpty(calculations.FirstOrDefault().Traveller.LastName) ? calculations.FirstOrDefault().Traveller.FirstName + " " + calculations.FirstOrDefault().Traveller.LastName : calculations.FirstOrDefault().Traveller.FirstName;
                }

                rptTripItems.DataSource = null;
                rptTripItems.DataSource = calculations;
                rptTripItems.DataBind();
            }
            else
            {
                lblMessageUser.Text = "There are no enteries for this trip.";
                lblMessageUser.Visible = true;
            }
        }

        protected void SetTripRepeater(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                try
                {
                    TripCalculations t = (TripCalculations)e.Item.DataItem;

                    Literal litName = (Literal)e.Item.FindControl("litName");
                    Literal litTotal = (Literal)e.Item.FindControl("litTotal");
                    Literal litOwed = (Literal)e.Item.FindControl("litOwed");
                    Literal litOwedTo = (Literal)e.Item.FindControl("litOwedTo");


                    litName.Text = t.Traveller.FirstName + " " + t.Traveller.LastName;
                    litTotal.Text = String.Format("{0:0.00}", t.Traveller.TotalPaid);
                    litOwed.Text = String.Format("{0:0.00}", t.AveragePaidByTravellers - t.Traveller.TotalPaid);
                    litOwedTo.Text = t.BiggestSpender; 

                }
                catch (Exception ex)
                {
                    _logger.Fatal("SetTripRepeater", ex);

                    throw ex;
                }
            }
        }
    }
}