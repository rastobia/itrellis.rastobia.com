﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace itrellis.rastobia.com.Model
{
    public class TripCalculations
    {
        public int? NumberOfTravellers { get; set; } 
        public decimal? AveragePaidByTravellers { get; set; }
        public Traveller Traveller { get; set; }
        public string BiggestSpender { get; set; }
    }
}
