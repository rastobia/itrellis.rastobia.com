﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace itrellis.rastobia.com.Model
{
    public class TripItems
    {
        public int? TripItemID { get; set; } //TripItems.TripItemID
        public int? TripID { get; set; } //TripItems.TripID
        public decimal? Amount { get; set; } //TripItems.Amount
        public string Currency { get; set; } //TripItems.Currency
        public string Description { get; set; } //TripItems.Description
        public int? TravellerID { get; set; } //TripItems.TravellerID
    }
}
