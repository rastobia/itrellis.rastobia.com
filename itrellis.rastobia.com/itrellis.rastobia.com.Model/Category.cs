﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace itrellis.rastobia.com.Model
{
    public class Category
    {
        public int? CategoryID { get; set; } //Category.CategoryID
        public string Description { get; set; } //Category.Description 
    }
}
