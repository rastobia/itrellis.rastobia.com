﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace itrellis.rastobia.com.Model
{
    public class Traveller
    {
        public int? TravellerID { get; set; } //Travellers.TravellerID
        public string FirstName { get; set; } //Travellers.FirstName
        public string LastName { get; set; } //Travellers.LastName
        public decimal TotalPaid { get; set; }   
    }
}
