﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace itrellis.rastobia.com.Model
{
    /// <summary>
    /// Trip Summary Information
    /// </summary>
    public class Trip
    {
        public int? TripID { get; set; } //Trip.TripID
        public DateTime? CreateStamp { get; set; } //Trip.CreateStamp

        public TripItems Item { get; set; }

        public Traveller Traveller { get; set; }
    }
}
